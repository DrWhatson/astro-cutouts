import sys
import pyvo as vo
from astropy import units as u
from astropy.coordinates import Angle
from astropy.coordinates import SkyCoord
import pandas
import json
import click


#@click.group()
#def cli():
#    pass
    
@click.command()
@click.argument("obj_list_file", type=click.File("rb"))
def cli(obj_list_file):
    # Read in sources to plot from CSV
    print(f"Reading source list file {obj_list_file}")
    srcs = pandas.read_csv(obj_list_file)
    nsrc = len(srcs)
    print(f"{nsrc} sources read")

    pos = []
    box = []
    src = [] 
    for i in range(nsrc):
        ra = srcs.loc[i][1]*u.deg
        dec = srcs.loc[i][2]*u.deg
        siz = srcs.loc[i][3]
        #print(i,ra,dec) # check
        pos.append(SkyCoord(ra,dec))
        box.append(siz)
        src.append(srcs.loc[i][0])

    url = "https://vo.astron.nl/lotss_dr2/q/query_mosaics/siap.xml?"

    res = []
    image_pos={} # Get running store of image centre positions
    for i in range(len(pos)):
        results = vo.dal.imagesearch(url, pos[i], size=box[i])
        tables = results.to_table()
        print(src[i])

        sep = []
        # Go through images checking for centre position
        for tab in tables:
            if tab[0] not in image_pos:
                ra = tab[3]*u.deg
                dec = tab[4]*u.deg
                image_pos[tab[0]] = SkyCoord(ra,dec)

            sep.append(pos[i].separation(image_pos[tab[0]]))

        min_sep = min(sep)
        min_pos = sep.index(min_sep)
#        print(sep,min_pos)
        r = [results[min_pos].getdataurl()]

        res.append((srcs.loc[i][0],r)) 

    # Write to JSON file
    with open('data.json', 'w') as outfile:
        json.dump(res, outfile)

    sys.exit(0)
